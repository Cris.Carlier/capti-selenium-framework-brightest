package com.selenium.pruebas;

import com.selenium.operaciones.OperacionesBase;
import com.selenium.operaciones.UtilesCompras;
import org.junit.Test;

public class ComprasAutoPractice extends OperacionesBase {
    private UtilesCompras utilesCompras = new UtilesCompras();

    @Test
    public void TC01_VentaEcommerce_Producto_BlueTop() {
        // Realizamos con producto "Blue Top".
        utilesCompras.VentaConProducto("Blue Top");
	}

    @Test
    public void TC02_VentaEcommerce_Producto_MenTshirt() {
        // Realizamos con producto "Men Tshirt".
        utilesCompras.VentaConProducto("Men Tshirt");
    }
}