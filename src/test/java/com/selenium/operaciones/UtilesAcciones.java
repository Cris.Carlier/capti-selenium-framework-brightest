package com.selenium.operaciones;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import java.util.Properties;

public class UtilesAcciones {
	private static WebDriver driver = OperacionesBase.driver;
	private static Properties properties = OperacionesBase.properties;

	public UtilesAcciones() {

	}

	public void Click(WebElement elemento){
		try {
			// Hacemos una breve pausa antes de realizar alguna accion.
			Esperar(1);

			// Hacemos click en el elemento.
			elemento.click();
		}

		catch (Exception Ex) {
			// En caso de error arrojamos un mensaje.
			ErrorJava("Hubo un error al hacer click en el elemento: " + elemento, Ex);
		}
	}

	public void Navegar(String propertyUrl){
		try {
			// Hacemos una breve pausa antes de realizar alguna accion.
			Esperar(1);

			// Hacemos click en el elemento.
			driver.get(properties.getProperty(propertyUrl));
		}

		catch (Exception Ex) {
			// En caso de error arrojamos un mensaje.
			Ex.printStackTrace();
		}
	}

	public void Escribir(WebElement elemento, String Texto){
		try {
			// Hacemos una breve pausa antes de realizar alguna accion.
			Esperar(1);

			// Ingresamos el texto indicado en el elemento.
			elemento.sendKeys(Texto);
		}

		catch (Exception Ex) {
			// En caso de error arrojamos un mensaje.
			ErrorJava("Hubo un error al ingresar el texto: " + Texto + ", en el elemento con la ruta: " + elemento, Ex);
		}
	}

	public String BuscarTexto(WebElement elemento){
		// Iniciamos la variable para almacenar el texto leido.
		String Lectura = null;

		try {
			// Hacemos una breve pausa antes de realizar alguna accion.
			Esperar(1);

			// Ingresamos el texto indicado en el elemento.
			Lectura = elemento.getText();
		}

		catch (Exception Ex) {
			// En caso de error arrojamos un mensaje.
			ErrorJava("Hubo un error al leer el texto en el elemento con la ruta: " + elemento, Ex);
		}

		// Devolvemos el texto encontrado.
		return Lectura;
	}

	public void ErrorJava(String Error, Exception Ex){
		try {
			// Arrojamos mensaje de error cuando sea indicado.
			throw new Exception(Error);
		}

		catch (Exception Err) {
			Err.printStackTrace();
		}
	}

	public void Esperar(int segundos) {
		try {
			// Hacemos una breve pausa antes de realizar alguna accion.
			Thread.sleep(1000 * segundos);
		}

		catch (Exception Ex) {
			// En caso de error arrojamos un mensaje.
			Ex.printStackTrace();
		}
	}

	public void AceptarAlerta() {
		Alert alert = driver.switchTo().alert();

		// Cierra la alerta haciendo clic en el botón "Aceptar"
		alert.accept();
	}
}
