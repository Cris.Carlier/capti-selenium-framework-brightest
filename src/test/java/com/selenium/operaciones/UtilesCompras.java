package com.selenium.operaciones;

import com.selenium.objetosPaginas.AutoPracticePage;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.Properties;

public class UtilesCompras {
	private UtilesAcciones utilesAcciones = new UtilesAcciones();
	private AutoPracticePage autoPracticePage = new AutoPracticePage();

	private static WebDriver driver = OperacionesBase.driver;
	private static Properties properties = OperacionesBase.properties;

	public UtilesCompras() {

	}

	public void ProcesoLoginUsuario() {
		utilesAcciones.Click(autoPracticePage.homePage_login);

		utilesAcciones.Escribir(autoPracticePage.loginPage_email, properties.getProperty("loginEmail"));

		utilesAcciones.Escribir(autoPracticePage.loginPage_pass, properties.getProperty("loginPass"));

		utilesAcciones.Click(autoPracticePage.loginPage_loginButton);
	}

	public void ProcesoAgregarProductoCarro(String producto) {
		try {
			WebElement elemento = driver.findElement(By.xpath("(//p[contains(text(),'" + producto + "')])[1]//following-sibling::a"));
			utilesAcciones.Click(elemento);

			utilesAcciones.Click(autoPracticePage.cartModal_continueShopping);
		}
		catch (Exception Err) {
			Err.printStackTrace();
		}
	}

	public void ProcesoCarroDeComprasCheckout(String producto) {
		utilesAcciones.Click(autoPracticePage.homePage_cart);

		WebElement elemento = driver.findElement(By.xpath("(//a[normalize-space()='" + producto + "'])[1]"));
		String validarProductoOrden = utilesAcciones.BuscarTexto(elemento);
		Assert.assertTrue("Error, no se encontro el producto en el carro", validarProductoOrden.contains(producto));

		utilesAcciones.Click(autoPracticePage.shoppingCartPage_proceedToCheckout);
		utilesAcciones.Click(autoPracticePage.checkoutPage_placeOrder);
	}

	public void ProcesoMedioDePago() {
		utilesAcciones.Escribir(autoPracticePage.paymentPage_cardName, properties.getProperty("paymentCardName"));

		utilesAcciones.Escribir(autoPracticePage.paymentPage_cardNumber, properties.getProperty("paymentCardNumber"));

		utilesAcciones.Escribir(autoPracticePage.paymentPage_cvc, properties.getProperty("paymentCvc"));

		utilesAcciones.Escribir(autoPracticePage.paymentPage_expiration, properties.getProperty("paymentExpiration"));

		utilesAcciones.Escribir(autoPracticePage.paymentPage_year, properties.getProperty("paymentYear"));

		utilesAcciones.Click(autoPracticePage.paymentPage_payConfirmOrder);
	}

	public void ProcesoFinalizacionCompra() {
		String validarMensajeOrden = utilesAcciones.BuscarTexto(autoPracticePage.orderPlacedPage_message);
		Assert.assertTrue("Error, no se pudo verificar que la venta haya finalizaod con exito", validarMensajeOrden.contains("Congratulations! Your order has been confirmed"));

		utilesAcciones.Click(autoPracticePage.orderPlacedPage_continue);
	}

	public void VentaConProducto(String producto) {
		// Navegamos a la pagina de la tienda e-commerce.
		utilesAcciones.Navegar("urlAutoPractice");

		// Nos autenticamos en el sitio para proceder con las compras.
		ProcesoLoginUsuario();

		// Nos dirigimos al catalogo de productos.
		autoPracticePage.AbrirCatalogoProductos();

		// Seleccionamos el producto indicado y lo agregamos al carro de compras.
		ProcesoAgregarProductoCarro(producto);

		// Procedemos con el carro de compra y el checkout.
		ProcesoCarroDeComprasCheckout(producto);

		// Ingresamos los datos y confirmamos el medio de pago
		ProcesoMedioDePago();

		// Validamos que la venta haya finalizado correctamente.
		ProcesoFinalizacionCompra();
	}
}
