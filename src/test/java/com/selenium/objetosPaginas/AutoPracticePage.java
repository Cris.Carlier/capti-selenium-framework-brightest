package com.selenium.objetosPaginas;

import com.selenium.operaciones.OperacionesBase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AutoPracticePage {
	WebDriver driver;

	@FindBy(xpath = "//*[@id='header']/div/div/div/div[2]/div/ul/li[2]/a")
	public WebElement homePage_products;

	@FindBy(xpath = "//*[@id='header']/div/div/div/div[2]/div/ul/li[4]/a")
	public WebElement homePage_login;

	@FindBy(xpath = "//*[@id='header']/div/div/div/div[2]/div/ul/li[3]/a")
	public WebElement homePage_cart;

	@FindBy(xpath = "//*[@id='cartModal']/div/div/div[3]/button")
	public WebElement cartModal_continueShopping;

	@FindBy(xpath = "//*[@id='do_action']/div[1]/div/div/a")
	public WebElement shoppingCartPage_proceedToCheckout;

	@FindBy(xpath = "//*[@id='form']/div/div/div[1]/div/form/input[2]")
	public WebElement loginPage_email;

	@FindBy(xpath = "//*[@id='form']/div/div/div[1]/div/form/input[3]")
	public WebElement loginPage_pass;

	@FindBy(xpath = "//*[@id='form']/div/div/div[1]/div/form/button")
	public WebElement loginPage_loginButton;

	@FindBy(xpath = "//*[@id='cart_items']/div/div[7]/a")
	public WebElement checkoutPage_placeOrder;

	@FindBy(xpath = "//*[@id='payment-form']/div[1]/div/input")
	public WebElement paymentPage_cardName;

	@FindBy(xpath = "//*[@id='payment-form']/div[2]/div/input")
	public WebElement paymentPage_cardNumber;

	@FindBy(xpath = "//*[@id='payment-form']/div[3]/div[1]/input")
	public WebElement paymentPage_cvc;

	@FindBy(xpath = "//*[@id='payment-form']/div[3]/div[2]/input")
	public WebElement paymentPage_expiration;

	@FindBy(xpath = "//*[@id='payment-form']/div[3]/div[3]/input")
	public WebElement paymentPage_year;

	@FindBy(xpath = "//*[@id='submit']")
	public WebElement paymentPage_payConfirmOrder;

	@FindBy(xpath = "//*[@id='form']/div/div/div/p")
	public WebElement orderPlacedPage_message;

	@FindBy(xpath = "//*[@id='form']/div/div/div/div/a")
	public WebElement orderPlacedPage_continue;

	public void AbrirCatalogoProductos() {
		homePage_products.click();
	}

	public AutoPracticePage() {
		this.driver = OperacionesBase.driver;
        PageFactory.initElements(driver, this);
	}
}