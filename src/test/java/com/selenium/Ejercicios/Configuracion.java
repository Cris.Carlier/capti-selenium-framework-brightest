package com.selenium.Ejercicios;

public class Configuracion
{
    // Para el propósito de este curso, mantendremos estas cadenas en una clase de Configuración como variables estáticas. Como regla general, usaremos una sintaxis de variable C Global para nombrar. Por ejemplo:
    public static String APP_URL = "https://practicetestautomation.com/practice-test-login/";
    public static String TEST_URL = "https://rahulshettyacademy.com/AutomationPractice/";
    public static String USER_NAME = "student";
    public static String USER_PASS = "Password123";
}