package com.selenium.Ejercicios;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class EjerciciosBack
{
	public static void main(String[] args) throws Exception
	{
		// Necesitamos decirle a Selenium dónde encontrar el ejecutable del navegador:
		// System.setProperty("webdriver.gecko.driver", "drivers/geckodriver.exe");
		System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");

		// Ahora podemos lanzar Firefox creando un objeto de FirefoxDriver:
		// FirefoxDriver driver = new FirefoxDriver();

		// Es una buena práctica mantener el tipo driver como interfaz de WebDriver, ya que te permite cambiar el navegador concreto sin cambiar el resto del código:
		// WebDriver driver = new FirefoxDriver();
		WebDriver driver = new ChromeDriver();
		Thread.sleep(1000);

		// Maximizar una ventana:
		// driver.manage().window().maximize();
		// Thread.sleep(1000);

		// Navegar a una URL
		driver.get(Configuracion.APP_URL);
		Thread.sleep(1000);

		/*WebElement element = driver.findElement(By.id("submit"));
		System.out.println(element);*/

		/*** CSS Selector ***/

		WebElement userName, passWord;

		// userName = driver.findElement(By.cssSelector("div[id*='or'] > div:nth-child(2) > input"));
		// userName = driver.findElement(By.cssSelector("div[id^='fo'] > div + div > input"));
		// userName = driver.findElement(By.cssSelector("div[id$='rm'] > div:nth-child(2) > input"));
		// userName = driver.findElement(By.cssSelector("div[id~='form'] > div > input"));

		userName = driver.findElement(By.cssSelector("input[id='username'],[name='username1']"));
		passWord = driver.findElement(By.cssSelector("div[id~='form'] > div:nth-child(2) > input"));

		userName.sendKeys("student ");
		passWord.sendKeys("Password123");

		// Salir de todas las ventanas del navegador
		Thread.sleep(1000);
		driver.quit();

		// Actions actions = new Actions(driver);
		// actions.sendKeys(userName, "test").perform();

		// Navegación driver.navigate()

		// Regresar
		// driver.navigate().back();

		// Continuar
		// driver.navigate().forward();

		// Actualizar
		// driver.navigate().refresh();

		// Obtener el acceso la ventana actual y todos los acceso a ventanas (window handles).
		// driver.getWindowHandle();
		// driver.getWindowHandles();

		// Obtener URL actual
		/*String urlActual = driver.getCurrentUrl();
		System.out.println("Url Actual: " + urlActual);
		Thread.sleep(3000);*/

		// Obtener el título de la página
		/* String tituloActual = driver.getTitle();
		System.out.println("Titulo Actual: " + tituloActual);
		Thread.sleep(3000); */

		/*
		// Obtener el origen de la página
		String origenActual = driver.getPageSource();
		System.out.println("Origen Actual: " + origenActual);*/




		// Salir de todas las ventanas del navegador
		// driver.quit();

		// Excepciones:
		/*String tituloEsperado = "Test Login | Practice Test Automation 2";
		throw new Exception(
				String.format(
						"ERROR: El titulo no es el mismo. Se esperaba: <%s> y el titulo actual es: <%s>",
						tituloEsperado,
						tituloActual
				)
		);
		*/
	}
}
