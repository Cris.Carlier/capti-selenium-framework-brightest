package com.selenium.Ejercicios;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.FileInputStream;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class Ejercicios
{
	public static void main(String[] args) throws Exception
	{
		System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		driver.get(Configuracion.TEST_URL);
		//driver.get(Configuracion.APP_URL);
		Thread.sleep(1000);

		WebElement testElement1, testElement2, testElement3, userName, passWord;

		/** Informacion de un Elemento **/

		/* String elementInfo;

		testElement1 = driver.findElement(By.xpath("//*[@id='name']"));
		testElement2 = driver.findElement(By.xpath("/html/body/h1"));
		testElement3 = driver.findElement(By.xpath("//*[@id='checkBoxOption1']"));

		//elementInfo = testElement1.getAttribute("value");
		elementInfo = testElement2.getText();
		//elementInfo = testElement3.getTagName();
		//elementInfo = testElement3.isDisplayed();
		//elementInfo = testElement3.isEnabled();
		//elementInfo = testElement3.isSelected();

		System.out.println("Info: " + elementInfo); */

		/** CSS Selector **/

		/* userName = driver.findElement(By.cssSelector("div[id*='or'] > div:nth-child(2) > input"));
		//userName = driver.findElement(By.cssSelector("div[id^='fo'] > div + div > input"));
		//userName = driver.findElement(By.cssSelector("div[id$='rm'] > div:nth-child(2) > input"));
		//userName = driver.findElement(By.cssSelector("div[id~='form'] > div > input"));

		//userName = driver.findElement(By.cssSelector("input[id='username'],[name='username1']"));
		passWord = driver.findElement(By.cssSelector("div[id~='form'] > div:nth-child(2) > input")); */

		/** XPath **/

		/* userName = driver.findElement(By.xpath("//input[@id='username']"));
		//userName = driver.findElement(By.xpath("//*[@id='form']/div/input"));
		//userName = driver.findElement(By.xpath("//input[contains(@id, 'serna')]"));
		//userName = driver.findElement(By.xpath("//input[starts-with(@id, 'userna')]"));
		//userName = driver.findElement(By.xpath("//label[normalize-space()='Username']/following-sibling::input"));

		//passWord = driver.findElement(By.xpath("//input[@id='password']"));
		//passWord = driver.findElement(By.xpath("//*[@id='form']/div/following-sibling::div/input"));

		userName.sendKeys(Configuracion.USER_NAME);
		passWord.sendKeys(Configuracion.USER_PASS); */

		/** Espera Dinamica **/

		/* boolean resultado = false;

		for (int i=0; i<5; i++) {
			System.out.println("Esperando resultado... intento " + i+1);
			Thread.sleep(500);

			if (resultado == true) {
				break;
			} else if (i >= 4) {
				throw new Exception("Error: tiempo de espera superado!");
			}
		} */

		/** Espera Implicita **/

		// driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

		/** Espera Explicita **/

		/* WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.titleIs("Practice Page2")); */

		/** Listas Desplegables **/

		/* WebElement testSelect = driver.findElement(By.xpath("//*[@id='dropdown-class-example']"));
		testSelect.sendKeys("Option1");

		Select select = new Select(testSelect);
		select.selectByVisibleText("Option1");
		select.selectByIndex(2);
		select.selectByValue("option3"); */

		/** Combinación de Varios Elementos **/

		/* List<WebElement> elementos = driver.findElements(By.xpath("//input[@type='checkbox']"));

		for (WebElement elemento: elementos) {
			Thread.sleep(750);

			if (elemento.isSelected()) {
				continue;
			} else {
				elemento.click();
			}
		} */

		/**  Elementos Anidados **/

		/* WebElement listaCheckbox = driver.findElement(By.xpath("/html/body/div[1]/div[4]/fieldset"));
		WebElement listaCheckbox_01 = listaCheckbox.findElement(By.xpath("//input[@id='checkBoxOption1']"));
		listaCheckbox_01.click(); */

		/**  Subir Archivos **/

		/* driver.get("C:\\capti-selenium-framework-main\\src\\test\\java\\com\\selenium\\Ejercicios\\upload_test.html");
		Thread.sleep(1000);

		WebElement fileUpload = driver.findElement(By.xpath("//*[@id='fileToUpload']"));
		fileUpload.sendKeys("C:\\capti-selenium-framework-main\\src\\test\\java\\com\\selenium\\Ejercicios\\txt_test.txt"); */

		/**  Ejecutando JavaScript **/

		/* JavascriptExecutor js = (JavascriptExecutor) driver;
		WebElement listaCheckbox_01 = driver.findElement(By.xpath("//input[@id='checkBoxOption1']"));
		js.executeScript("arguments[0].click();", listaCheckbox_01); */

		/**  Manejo de Ventanas/Pestañas **/

		/* Set<String> ventanas = driver.getWindowHandles();
		System.out.println(ventanas);

		WebElement newTab = driver.findElement(By.xpath("//*[@id='opentab']"));
		newTab.click();

		ventanas = driver.getWindowHandles();
		System.out.println(ventanas);

		driver.switchTo().window(ventanas[0]); */

		// driver.switchTo().window(<handleString>.
		/* Actions actions = new Actions(driver);
		//actions.sendKeys(userName, "test").perform(); */

		Thread.sleep(1000);
		driver.quit();
	}
}
